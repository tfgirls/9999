import tkinter as tk
import time

class CountdownTimer:
    def __init__(self, master, minutes=0, seconds=0):  # 添加默认倒计时
        self.master = master
        master.title("Countdown Timer")
        master.geometry("350x200")  # 将gui界面调整为适当大小

        self.minutes = tk.StringVar(value=str(minutes))  # 设置默认值
        self.seconds = tk.StringVar(value=str(seconds))  # 设置默认值

        # 创建分钟和秒钟输入框
        tk.Label(master, text="min:").grid(row=0, column=0)
        tk.Entry(master, textvariable=self.minutes, width=10).grid(row=0, column=1)
        tk.Label(master, text="sec:").grid(row=0, column=2)
        tk.Entry(master, textvariable=self.seconds, width=10).grid(row=0, column=3)

        # 创建启动，暂停，延长和退出按钮
        self.start_button = tk.Button(master, text="Advance", command=self.start_timer)
        self.start_button.grid(row=3, column=0, pady=10)
        self.pause_button = tk.Button(master, text="Pause", command=self.pause_timer, state=tk.DISABLED)
        self.pause_button.grid(row=3, column=2, pady=10)
        self.extend_button = tk.Button(master, text="Extend", command=self.extend_timer, state=tk.DISABLED)
        self.extend_button.grid(row=3, column=4, pady=10)


        self.remaining_time = 0
        self.timer_running = False

        # 添加倒计时标签
        self.countdown_label = tk.Label(master, text="", font=("Arial", 20, "bold"))
        self.countdown_label.grid(row=5, column=0, columnspan=4, sticky="S", pady=10)

    def start_timer(self):
        if not self.timer_running:
            self.remaining_time = int(self.minutes.get()) * 60 + int(self.seconds.get())
            self.timer_running = True
            self.start_button.config(state=tk.DISABLED)
            self.pause_button.config(state=tk.NORMAL)
            self.extend_button.config(state=tk.NORMAL)
            self.update_timer()

    def pause_timer(self):
        if self.timer_running:
            self.timer_running = False
            self.start_button.config(state=tk.NORMAL)
            self.pause_button.config(state=tk.DISABLED)
            self.extend_button.config(state=tk.DISABLED)

    def extend_timer(self):
        if self.timer_running:
            self.start_button.config(state=tk.NORMAL)
            self.pause_button.config(state=tk.NORMAL)
            self.extend_button.config(state=tk.DISABLED)


    def update_timer(self):
        if self.timer_running:
            minutes, seconds = divmod(self.remaining_time, 60)
            self.minutes.set("{:02d}".format(minutes))
            self.seconds.set("{:02d}".format(seconds))
            self.countdown_label.config(text=f"{minutes:02d}:{seconds:02d}",
                                        font=("Arial", 16, "bold"))  # 优化1: 将倒计时标签的字体大小调整为适当大小
            if self.remaining_time == 0:
                self.timer_running = False
                self.start_button.config(state=tk.NORMAL)
                self.pause_button.config(state=tk.DISABLED)
            else:
                self.remaining_time -= 1
                self.master.after(1000, self.update_timer)

root = tk.Tk()
timer = CountdownTimer(root)
root.mainloop()